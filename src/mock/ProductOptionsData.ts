const ProductIngredientsOptions = [
  { name: 'Peproni', price: 0},
  { name: 'Mozerrealla Cheese', price: 0 },
  { name: 'Basil', price: 0 },
  { name: 'Oregano', price: 0 },
  { name: 'Chilli', price: 0 },
]

const ProductExtraIngredientsOptions = [
  { name: 'Peproni', price: 2 },
  { name: 'Mozerrealla Cheese', price: 2  },
  { name: 'Basil', price: 4  },
  { name: 'Oregano', price: 1  },
  { name: 'Chilli', price: 5  },
]

export { ProductIngredientsOptions, ProductExtraIngredientsOptions }
