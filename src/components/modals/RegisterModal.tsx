import React from 'react'
import ProfileIcon from '../../assets/icons/profile.png'
import NameIcon from '../../assets/icons/person-black.png'
import CloseTag from '../CloseTag'
import EmailIcon from '../../assets/icons/email.png'
import PhoneIcon from '../../assets/icons/phone.png'
import PasswordIcon from '../../assets/icons/password.png'
import { connect } from 'react-redux'
import { Register } from '../../redux/user/types'
import { registerUser, remove } from '../../redux/user/actions'
import { withTranslation, WithTranslation } from 'react-i18next'
import { ValidateEmail, ValidatePhone } from '../../helpers/validator'


interface Props extends WithTranslation {
  onClose?(): void

  visible: boolean

  onAlreadyHaveAnAccount?(): void

  Signup: (user: Register) => void
  removeError: any
}

interface State {
  email: string;
  phone: string;
  password: string;
  confirmPassword: string;
  login: string;
  firstName: string;
  lastName: string;
  formErrors: any[]
}

class RegisterModal extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      email: '',
      phone: '',
      password: '',
      confirmPassword: '',
      login: '',
      firstName: '',
      lastName: '',
      formErrors: [],
    }
  }

  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
      formErrorMessage: '',
    } as Pick<State, any>)
    this.props.removeError()
  }
  registerHandler = () => {
    const { email, firstName, lastName, password, login, confirmPassword, phone } = this.state
    const { t } = this.props
    const formErrors: any[] = [];
    if (login === '') formErrors.push('username')
    if (firstName === '') formErrors.push('firstName')
    if (lastName === '') formErrors.push('lastName')
    if (email === '') formErrors.push('email')
    else if (!ValidateEmail(email)) formErrors.push('wrongEmail')
    if (phone === '') formErrors.push('phone')
    else if (!ValidatePhone(phone)) formErrors.push('wrongPhone')
    if (password === '') formErrors.push('password')
    if (confirmPassword === '') formErrors.push('confirmPassword')
    else if (password !== confirmPassword) formErrors.push('match')
    if (formErrors.length === 0) {
      const user = {
        login,
        email,
        password,
      }
      this.props.Signup(user)
    }
    this.setState({ formErrors });
  }
  formErrorMessage = (field) => {
    if (this.state.formErrors.length > 0 && this.state.formErrors.includes(field)) {
      const errorCheck = () => {
        if (field === 'match') return 'Please enter the same value again.'
        else if (field === 'wrongEmail') return 'Please enter a valid email address'
        else if (field === 'wrongPhone') return 'Please enter a valid phone number'
        else return 'This field is required.'
      }
      return (
        <h4 className={'alert-text'}>
          {errorCheck()}
        </h4>
      )
    }
  }

  render() {
    const { onClose, visible, onAlreadyHaveAnAccount } = this.props
    const { t } = this.props
    const { login, firstName, lastName, email, password, confirmPassword, phone } = this.state
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body'}>
          <div className={'header'}>
            <CloseTag onClose={onClose} />
            <img src={ProfileIcon} alt={'Profile'} className={'main-icon'} />
            <h3 className={'title'}>{t('CREATE YOUR ACCOUNT')}</h3>
          </div>
          <div className={'content'}>
            <div className="field-container">
              <img src={NameIcon} alt="User Name" />
              <div>
                {this.formErrorMessage('username')}
                <input type="text" placeholder={t('USER NAME')} name="login" value={login} onChange={this.onChangeHandler}
                  className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={NameIcon} alt="First Name" />
              <div>
                {this.formErrorMessage('firstName')}
                <input type="text" placeholder={t('FIRST NAME')} name="firstName" value={firstName} onChange={this.onChangeHandler}
                  className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={NameIcon} alt="Last Name" />
              <div>
                {this.formErrorMessage('lastName')}
                <input type="text" placeholder={t('LAST NAME')} name="lastName" value={lastName} onChange={this.onChangeHandler}
                  className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={EmailIcon} alt="Email" />
              <div>
                {this.formErrorMessage('email')}
                {this.formErrorMessage('wrongEmail')}
                <input type="email" placeholder={t('EMAIL')} name="email" value={email} onChange={this.onChangeHandler}
                  className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={PhoneIcon} alt="Phone Number" />
              <div>
                {this.formErrorMessage('phone')}
                {this.formErrorMessage('wrongPhone')}
                <input type="text" placeholder={t('PHONE NUMBER')} name="phone" value={phone} onChange={this.onChangeHandler}
                  className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={PasswordIcon} alt="Password" />
              <div>
                {this.formErrorMessage('password')}
                <input type="password" placeholder={t('PASSWORD')} name="password" value={password}
                  onChange={this.onChangeHandler} className={'no-margin'} />
              </div>
            </div>
            <div className="field-container">
              <img src={PasswordIcon} alt="Confirm Password" />
              <div>
                {this.formErrorMessage('confirmPassword')}
                {this.formErrorMessage('match')}
                <input type="password" placeholder={t('CONFIRM PASSWORD')} name="confirmPassword" value={confirmPassword}
                  onChange={this.onChangeHandler} className={'no-margin'} />
              </div>
            </div>
            <div className={'hr'} />
            <div className={'button'} onClick={this.registerHandler}>{t('SIGNUP')}</div>
            <div className={'sub-link center'} onClick={onAlreadyHaveAnAccount}>
              <em>{t('Already have an account')}</em>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const comp = connect((state: any) => {

}, (dispatch: any) => {
  return {
    Signup: (create: Register) => dispatch(registerUser(create)),
    removeError: () => dispatch(remove()),
  }
})(RegisterModal)

export default withTranslation()(comp)
