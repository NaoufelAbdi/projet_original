import React from 'react'
import TickIcon from '../../assets/icons/tick-white.png'

interface Props {
  visible: boolean
  close(): void
}
interface State {
}

class UpdateModel extends React.Component<Props, State> {
  render(): React.ReactNode {
    const {
      visible,
    } = this.props
    return (
      <div className={`passwordChange-modal ${visible ? 'open' : ''}`}>
        <div className={'body'}>
          <div className={'green-icon'}>
            <img src={TickIcon} alt={'tick icon'} />
          </div>
          <h2>CUSTOMER DATA UPDATED!</h2>
        </div>
      </div>
    )
  }
}
export default UpdateModel