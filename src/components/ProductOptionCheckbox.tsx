import React from 'react'
import TickIcon from '../assets/icons/tick.png'
import Option from '../interfaces/Option'

interface CheckboxProps {
  active?: boolean,
  onToggle(options: Option): void
  option: Option
  label?: string,
  price?: number
}

const ProductOptionCheckbox = ({ active = true, onToggle, label, option, price }: CheckboxProps): JSX.Element => (
  <div className={'product-option-checkbox'}
    onClick={() => {
      onToggle(option)
    }}>
    <div>
      <div className={`checkbox ${active ? 'active' : 'non-active'}`}>
        {active &&
          <img
            src={TickIcon}
            alt="tick option"
            className={'tick-icon'}
          />}
      </div>
      <p>{label}</p>
    </div>
    {price && (
      <p>+${price}.00</p>
    )}
  </div>
)

export default ProductOptionCheckbox
