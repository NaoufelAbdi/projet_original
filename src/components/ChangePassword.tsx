import HorizontalLine from './HorizontalLine'
import React from 'react'
import EyeIcon from '../assets/icons/eye.png'
import LockIcon from '../assets/icons/lock.png'
import { withTranslation, WithTranslation } from "react-i18next";
import { connect } from 'react-redux'
import { PasswordInterface } from '../redux/user/types'
import { passwordChange } from '../redux/user/actions'
import Loader from 'react-loader-spinner'

interface Props extends WithTranslation {
  onNext(): void,
  oldPassCheck: boolean,
  PasswordChange: (passwordData: PasswordInterface) => void
  loading?: boolean
}
interface State {
  currentPassword: string,
  newPassword: string,
  comparePassword: boolean
  confirmPassword: string
  formErrorMessage: string
  hidePassword: boolean
  hideNewPassword: boolean
  hideConfirmPassword: boolean
}

class ChangePassword extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
      comparePassword: false,
      formErrorMessage: "",
      hidePassword: false,
      hideNewPassword: false,
      hideConfirmPassword: false
    }
  }
  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
      formErrorMessage: ""
    } as Pick<State, any>)
  }
  handleHidePassword = () => {
    this.setState({ hidePassword: !this.state.hidePassword })
  }
  handleHideNewPassword = () => {
    this.setState({ hideNewPassword: !this.state.hideNewPassword })
  }
  handleHideConfirmPassword = () => {
    this.setState({ hideConfirmPassword: !this.state.hideConfirmPassword })
  }
  passwordChangeHandler = () => {
    const { currentPassword, newPassword, confirmPassword, } = this.state
    if (currentPassword === '') {
      this.setState({ formErrorMessage: 'Please fill the current password' })
    } else if (newPassword === '') {
      this.setState({ formErrorMessage: 'Please fill the new password' })
    } else if (confirmPassword === '') {
      this.setState({ formErrorMessage: 'Please fill the new password' })
    } else if (newPassword !== confirmPassword) {
      this.setState({ formErrorMessage: 'Your password is not match' })
    } else {
      const passwordData = {
        currentPassword,
        newPassword,
      }
      this.props.PasswordChange(passwordData)
    }
    this.setState({
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    })
  }
  render() {
    const { t } = this.props
    const loading = this.props.loading
    const { currentPassword, newPassword, confirmPassword } = this.state
    return (
      <div className={'card detail'}>
        <h4>{t('CHANGE PASSWORD')}</h4>
        <HorizontalLine />
        {this.state.formErrorMessage.length > 0 &&
          <div className={'bt-alert'}>
            <h3>
              {this.state.formErrorMessage}
            </h3>
          </div>
        }
        <div className={'password'}>
          <div className={'password-bar'}>
            <img src={LockIcon} alt={'Lock icon'} />
            <input type={this.state.hidePassword ? "text" : "password"} placeholder={t('ENTER OLD PASSWORD')} name="currentPassword" value={currentPassword} onChange={this.onChangeHandler} />
            <div className={'circle-icon'}>
              <div className={'icon no-border'}>
                <img style={{ cursor: 'pointer' }} src={EyeIcon} alt={'Eye icon'} onClick={this.handleHidePassword} />
              </div>
            </div>
          </div>
          <HorizontalLine />
          {!this.props.oldPassCheck && (
            <div className={'submit-button'}>
              <button onClick={this.props.onNext}>
                {t('Next')}
              </button>
            </div>
          )}
          {this.props.oldPassCheck && (<div>
            <div className={'password-bar'}>
              <img src={LockIcon} alt={'Lock icon'} />
              <input type={this.state.hideNewPassword ? "text" : "password"} placeholder={t('ENTER NEW PASSWORD')} name="newPassword" value={newPassword} onChange={this.onChangeHandler} />
              <div className={'circle-icon'}>
                <div className={'icon no-border'}>
                  <img style={{ cursor: 'pointer' }} src={EyeIcon} alt={'Eye icon'} onClick={this.handleHideNewPassword} />
                </div>
              </div>
            </div>
            <div className={'password-bar'}>
              <img src={LockIcon} alt={'Lock icon'} />
              <input type={this.state.hideConfirmPassword ? "text" : "password"} placeholder={t('CONFIRM NEW PASSWORD')} name="confirmPassword" value={confirmPassword} onChange={this.onChangeHandler} />
              <div className={'circle-icon'}>
                <div className={'icon no-border'}>
                  <img style={{ cursor: 'pointer' }} src={EyeIcon} alt={'Eye icon'} onClick={this.handleHideConfirmPassword} />
                </div>
              </div>
            </div>
            <HorizontalLine />
            <div className={'submit-button'}>
              <button onClick={this.passwordChangeHandler}>
                {loading ? <div className={'loader-login-password'}><Loader
                  type="Oval"
                  color="white"
                  height={18}
                  width={18}
                  timeout={700000}
                /></div> :
                  <> {t('Submit')} </>
                }
              </button>
            </div>
          </div>)}
        </div>
      </div>)
  }
}
const comp = connect((state: any) => {
  return {
    loading: state.auth.loading
  };
}, (dispatch: any) => {
  return {
    PasswordChange: (passwordData: PasswordInterface) => dispatch(passwordChange(passwordData)),
  };
})(ChangePassword);

export default withTranslation()(comp)
