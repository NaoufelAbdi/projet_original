import React from 'react'
import OrderListContent from '../OrderListContent'
import { Product } from '../../redux/products/types'
import CartIcon from '../../assets/icons/cart.png'
import RightIcon from '../../assets/icons/right-icon-white.png'
import CloseTag from '../CloseTag'
import { CartItem, total } from '../../redux/cart/types'
import { connect } from 'react-redux'
import { grandTotalAction, removeProductFromCart, updateProductInCart } from '../../redux/cart/actions'
import { withTranslation, WithTranslation } from "react-i18next";

interface Props extends WithTranslation{
  onClose(): void,
  cart?: any
  auth?: any
  onAlsoLikeClick?(): void
  GrandTotalSave?(totalData: total): void
  onCheckoutClick?(): void
  onCustomerLogin?(): void
  onReceiptClick(product: Product): void
  visible: boolean
}

class OrderModalMobile extends React.Component<Props> {
  handleCheckOut = () => {
    this.props.onAlsoLikeClick();
  }
  render(): React.ReactNode {
    const { onClose, onReceiptClick, visible } = this.props
    // const { t } = this.props
    let { cart } = this.props
    let grandTotal = cart.cart.map((cartItem: CartItem) => {
      return cartItem.quantity * (cartItem.product.price + cartItem.totalPrice)
    })
    let GrandTotal = 0
    for (let i = 0; i < grandTotal.length; i++) {
      GrandTotal += grandTotal[i]
    }
    return (
      <div className={`order-modal-mobile ${visible ? 'open' : ''}`}>
        <CloseTag style={{right: 10}} onClose={onClose}/>
        <div className="body">
          <OrderListContent onReceiptClick={onReceiptClick} isMobile={true} />
        </div>
        <div className="footer">
            <div className={'cart-icon'}>
              <img src={CartIcon} alt={'Cart Icon'} className={'icon'}/>
            </div>
            <h2>{GrandTotal.toFixed(2)}</h2>
            <div className={'checkout'} onClick={this.handleCheckOut}>
              <h2>CHECKOUT</h2>
              <img src={RightIcon} alt={'next icon'}/>
            </div>
        </div>
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    cart: state.cart,
    auth: state.auth,
  };
}, (dispatch: any) => {
  return {
    updateProductInCart: (cartItem: CartItem) => dispatch(updateProductInCart(cartItem)),
    removeProductFromCart: (cartItem: CartItem) => dispatch(removeProductFromCart(cartItem.id || 0)),
    GrandTotalSave: (totalData: total) => dispatch(grandTotalAction(totalData)),
  };
})(OrderModalMobile);

export default withTranslation()(comp);

