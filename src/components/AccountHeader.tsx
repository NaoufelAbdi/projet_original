import React from 'react'
import LogoImg from '../assets/images/logokwik.png'
import { Link } from 'react-router-dom'
import Cross from '../assets/icons/cross-white.png'
import { WithTranslation, withTranslation } from 'react-i18next';


interface Props extends WithTranslation {
  onCartToggle?(): void
}



class AccountHeader extends React.Component<Props> {
  render(): React.ReactNode {
    const { t } = this.props;
    return (
      <header id={'account-top-banner'}>
        <div className={'logo-container'}>
          <Link to={'/'}>
            <img src={LogoImg} className={'logo'} alt={'Logo image'} />
          </Link>
        </div>
        <Link to={'/'}>
          <img src={Cross} className={"imageIconCross"} />
        </Link>
        <div className={'nav-buttons'}>
          <Link to={'/'}>
            {this.props.t('BACK TO HOME')}
          </Link>
        </div>
      </header>
    )
  }
}

export default withTranslation()(AccountHeader)
