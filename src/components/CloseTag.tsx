import React from 'react'
import CloseIcon from '../assets/icons/cross-primary.png';

interface Props {
  onClose?(): void
  style?: any
}

class CloseTag extends React.Component<Props> {
  render() {
    const { onClose, style } = this.props;
    return (
      <div className={'close-tag'} onClick={onClose} style={style || {}}>
        <img src={CloseIcon} alt={'Close'} />
      </div>
    );
  }
}

export default CloseTag;
