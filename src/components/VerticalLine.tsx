import React from 'react'

const VerticalLine = (): JSX.Element => (
  <div className={'vertical-line'} />
)

export default VerticalLine;
