import HorizontalLine from './HorizontalLine'
import React from 'react'
import { withTranslation, WithTranslation } from "react-i18next";
import ClockIcon from '../assets/icons/clock-primary.png'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { clickOrder, ReClickOrder } from '../redux/orders/actions'
import * as types from '../redux/user/types'

interface Props extends WithTranslation {
  orderDetails?(): void
  orders?: any
  handleDetails?(): void
  orderClick?: any
  ReOrderClick?: any
  userInfo: types.UserInfo
}
interface State {
  directHome: boolean
}
class OrderHistory extends React.Component<Props, State>{
  constructor(props: any) {
    super(props)
    this.state = {
      directHome: false
    }
  }
  handleDetails = (order): any => {
    this.props.orderClick(order)
    this.props.orderDetails()
  }
  handleReorder = (order): any => {
    this.props.ReOrderClick(order)
    this.setState({
      directHome: true
    })
  }
  render() {
    if (this.state.directHome) {
      return (<Redirect to={"/"} />);
    }
    const { email } = this.props.userInfo
    const { t } = this.props
    const orders = this.props.orders || []
    let ordersFilter = orders.filter((data) => {
      return data.email === email
    })
    return (
      <div className={'card detail'}>
        <h4>{t('YOUR ORDER HISTORY')}</h4>
        <HorizontalLine />
        <div className={'order-items'}>
          {ordersFilter.map((order, i) => {
            return (
              <div className={'order-horizontal'} >
                <div className={'order-item'}>
                  <p className={'pickup'}>
                    Pickup
                </p>
                  <div className={'order-number'}>
                    <h4>{order.id ? order.id : "_"}</h4>
                  </div>
                  <div className={'oder-sections'}>
                    <div className={'order-section-details'}>
                      <img src={ClockIcon} alt={'Profile'} className="icon-myaccount" />
                      <h1>{order.date ? order.date : "_"}</h1>
                    </div>
                    <div className={'order-section-details'}>
                      <img src={ClockIcon} alt={'Profile'} className="icon-myaccount" />
                      <h1>from Little Italy</h1>
                    </div>
                    <div className={'order-section-details'}>
                      <img src={ClockIcon} alt={'Profile'} className="icon-myaccount" />
                      <h1>${order.totalAmount ? order.totalAmount : "_"}</h1>
                    </div>
                  </div>
                  <div className={'order-buttons'}>
                    <button className={"button-details"} key={i} onClick={() => this.handleDetails(order)}>
                      Details
                  </button>
                    <button className={'button-reorder'} key={i} onClick={() => this.handleReorder(order)}>
                      Re-order
                    </button>
                  </div>
                </div>
              </div>
            )
          }
          )}
        </div>
      </div>)
  }
}
const comp = connect((state: any) => {
  return {
    orders: state.order.orders,
    userInfo: state.auth.userInfo,
  };
}, (dispatch: any) => {
  return {
    orderClick: (order: any) => dispatch(clickOrder(order)),
    ReOrderClick: (order: any) => dispatch(ReClickOrder(order)),
  };
})(OrderHistory);
export default withTranslation()(comp)
