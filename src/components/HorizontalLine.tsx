import React from 'react'

const HorizontalLine = (): JSX.Element => (
  <div className={'horizontal-line'} />
)

export default HorizontalLine;
